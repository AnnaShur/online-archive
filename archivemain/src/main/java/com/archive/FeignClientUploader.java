package com.archive;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

    @FeignClient(name="uploader",url = "http://localhost:8081")
    public interface FeignClientUploader {

        @GetMapping(value = "/niceday")
        String sayNiceDay();

        @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
        ResponseEntity<String> uploadFile(@RequestPart(required = true, value = "file")
                                                  MultipartFile file) throws IOException;

    }

