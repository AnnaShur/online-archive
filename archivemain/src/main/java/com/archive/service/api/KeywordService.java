package com.archive.service.api;

import com.archive.data.dto.ArticleResponseDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface KeywordService {
    List<ArticleResponseDto> getAllArticlesByKeyword(String word);
}
