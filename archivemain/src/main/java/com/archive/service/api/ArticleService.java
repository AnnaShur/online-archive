package com.archive.service.api;

import com.archive.data.dto.ArticleResponseDto;
import com.archive.data.entity.ArticleEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public interface ArticleService {
    List<ArticleResponseDto> getAllArticles();
    ArticleEntity save(Object article);
    void delete(Long id);
    ArticleEntity findArticleById(Long id);
    List<ArticleResponseDto> getByTimeInterval(String fromDate, String toDate);
    List<ArticleResponseDto> findAllByDateBetween(LocalDate fromDate, LocalDate toDate);
    ResponseEntity<String> upload(MultipartFile file)throws IOException;
    HttpEntity<MultiValueMap<String, Object>> createRequestEntity(MultipartFile file);
    ArticleResponseDto convertStringToArticleResponseDto(String artString)throws JsonMappingException, JsonProcessingException;



}