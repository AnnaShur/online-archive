package com.archive.service.api;

import com.archive.data.dto.ArticleResponseDto;
import com.archive.data.dto.AuthorResponseDto;
import com.archive.data.entity.ArticleEntity;
import com.archive.data.entity.AuthorEntity;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;


public interface AuthorService {
    List<ArticleResponseDto> getAllAuthorArticles(Long id);
    AuthorResponseDto getAuthorById(Long id);
    AuthorResponseDto getByFirstNameOrLastName(String firstName, String lastName);
    AuthorResponseDto findByLastNameLike(String lastName);
}
