package com.archive.service.impl;

import com.archive.data.dto.ArticleResponseDto;
import com.archive.data.dto.AuthorResponseDto;
import com.archive.data.entity.ArticleEntity;
import com.archive.data.entity.AuthorEntity;
import com.archive.data.mappers.ArticleMapper;
import com.archive.data.mappers.AuthorMapper;
import com.archive.repository.AuthorRepository;
import com.archive.service.api.AuthorService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    AuthorRepository authorRepository;


    private final AuthorMapper mapperAuthor = Mappers.getMapper(AuthorMapper.class);
    private final ArticleMapper mapperArticle = Mappers.getMapper(ArticleMapper.class);

    @Override
    public List<ArticleResponseDto> getAllAuthorArticles(Long id) {
        return authorRepository.findById(id)
                .orElseThrow(NoSuchElementException::new)
                .getArticles()
                .stream()
                .map(mapperArticle::articleEntityToArticleResponseDto)
                .collect(Collectors.toList());

    }

    @Override
    public AuthorResponseDto getAuthorById(Long id) {
        return mapperAuthor.authorEntityToAuthorResponseDto(authorRepository.findById(id).get());
    }

    @Override
    public AuthorResponseDto getByFirstNameOrLastName(String firstName,String lastName) {
     return mapperAuthor.authorEntityToAuthorResponseDto
             (authorRepository.findByLastNameIgnoreCaseOrFirstNameIgnoreCase(firstName,lastName));
    }

    @Override
    public AuthorResponseDto findByLastNameLike(String lastName) {
        return mapperAuthor.authorEntityToAuthorResponseDto
                (authorRepository.findByLastNameLike(lastName));
    }
}
