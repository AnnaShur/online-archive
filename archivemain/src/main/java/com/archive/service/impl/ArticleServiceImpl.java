package com.archive.service.impl;


import com.archive.FeignClientUploader;
import com.archive.data.dto.ArticleResponseDto;
import com.archive.data.entity.ArticleEntity;
import com.archive.data.mappers.ArticleMapper;
import com.archive.repository.ArticleRepository;
import com.archive.service.api.ArticleService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@Service("actualEntityService")
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    ArticleRepository artRepository;
    @Autowired
    FeignClientUploader feignUploader;

    private ArticleMapper mapper = Mappers.getMapper(ArticleMapper.class);


    @Override
    public List<ArticleResponseDto> getAllArticles() {
        List<ArticleResponseDto> artDtos = new ArrayList();
        for (ArticleEntity artDto : artRepository.findAll()) {
            artDtos.add(mapper.articleEntityToArticleResponseDto(artDto));
        }
        return artDtos;

    }

    @Override
    public ArticleEntity save(Object article) {
        if(article.getClass()==ArticleEntity.class)
            return artRepository.save((ArticleEntity) article);
        else
        return artRepository.save(mapper.articleResponseDtoToArticleEntity((ArticleResponseDto) article));

    }

    //TODO:Dto

   /* @Override
    public ArticleEntity saveOrUpdate(ArticleEntity article) {
        if (artRepository.findById(article.getId()).isPresent()) {
            ArticleEntity tmpEntity = artRepository.findById(article.getId()).get();
            tmpEntity.setTitle(article.getTitle());
            tmpEntity.setText(article.getText());
            tmpEntity.setAuthorId(article.getAuthorId());
            return artRepository.save(tmpEntity);
        } else
            return artRepository.save(article);


    }*/

    @Override
    public void delete(Long id) {
        if (artRepository.findById(id).isPresent())
            artRepository.deleteById(id);
        else throw new NoSuchElementException();

    }

    @Override
    public ArticleEntity findArticleById(Long id) {
        return artRepository.getById(id);
    }

    @Override
    public List<ArticleResponseDto> getByTimeInterval(String fromDate, String toDate) {
        return artRepository.findAll()
                 .stream()
                 .filter(entity -> entity.getDate().isAfter(LocalDate.parse(fromDate))
                     && entity.getDate().isBefore(LocalDate.parse(toDate)))
                 .map(mapper::articleEntityToArticleResponseDto)
                 .collect(Collectors.toList());
       /* List<ArticleResponseDto> articleList=new ArrayList<>();
        for (ArticleEntity tmpEntity : artRepository.findAll()) {
            if(tmpEntity.getDate().isAfter(LocalDate.parse(fromDate))
             && tmpEntity.getDate().isBefore(LocalDate.parse(toDate)))
                articleList.add(mapper.articleEntityToArticleResponseDto(tmpEntity));
        }
        return articleList;*/
    }

    @Override
    public List<ArticleResponseDto> findAllByDateBetween(LocalDate fromDate,LocalDate toDate) {
        return artRepository.findAllByDateBetween(fromDate,toDate)
                .stream()
                .map(mapper::articleEntityToArticleResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<String> upload(MultipartFile file) throws IOException {
        return feignUploader.uploadFile(file);
    }

    @Override
    public HttpEntity<MultiValueMap<String, Object>> createRequestEntity(MultipartFile file) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        Resource fileResource = file.getResource();
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", fileResource);
        return new HttpEntity<>(body, headers);
    }

    @Override
    public ArticleResponseDto convertStringToArticleResponseDto(String artString) throws JsonMappingException, JsonProcessingException {
        ObjectMapper objMap;
        objMap = JsonMapper.builder()
                .findAndAddModules()
                .build();
        return objMap.readValue(artString,ArticleResponseDto.class);
    }


}
