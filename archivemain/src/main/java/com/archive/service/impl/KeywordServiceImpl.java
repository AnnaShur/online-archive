package com.archive.service.impl;

import com.archive.data.dto.ArticleResponseDto;
import com.archive.data.dto.KeywordResponseDto;
import com.archive.data.mappers.ArticleMapper;
import com.archive.data.mappers.KeywordMapper;
import com.archive.repository.KeywordRepository;
import com.archive.service.api.KeywordService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class KeywordServiceImpl implements KeywordService {

    @Autowired
    KeywordRepository keywordRepository;

    private final ArticleMapper mapperArticle = Mappers.getMapper(ArticleMapper.class);

    @Override
    public List<ArticleResponseDto> getAllArticlesByKeyword(String word) {
        return  keywordRepository.findByWordIgnoreCase(word)
                .getArticles()
                .stream()
                .map(mapperArticle::articleEntityToArticleResponseDto)
                .collect(Collectors.toList());
    }
}
