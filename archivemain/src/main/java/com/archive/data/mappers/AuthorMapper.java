package com.archive.data.mappers;

import com.archive.data.dto.AuthorResponseDto;
import com.archive.data.entity.AuthorEntity;
import org.mapstruct.Mapper;


@Mapper
public interface AuthorMapper {
    AuthorResponseDto authorEntityToAuthorResponseDto(AuthorEntity entity);
    AuthorEntity authorResponseDtoToAuthorEntity(AuthorResponseDto dto);


}