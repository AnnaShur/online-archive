package com.archive.data.mappers;

import com.archive.data.dto.ArticleResponseDto;
import com.archive.data.entity.ArticleEntity;
import org.mapstruct.Mapper;

@Mapper
public interface ArticleMapper {

    ArticleResponseDto articleEntityToArticleResponseDto(ArticleEntity entity);
    ArticleEntity articleResponseDtoToArticleEntity(ArticleResponseDto dto);


}