package com.archive.data.mappers;

import com.archive.data.dto.KeywordResponseDto;
import com.archive.data.entity.KeywordEntity;
import org.mapstruct.Mapper;

@Mapper
public interface KeywordMapper {
    KeywordResponseDto KeywordEntityToKeywordResponseDto(KeywordEntity entity);
    KeywordEntity KeywordResponseDtoToKeywordEntity(KeywordResponseDto dto);
}
