package com.archive.data.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ArticleResponseDto {
    private String title;
    private Long authorId;
    private LocalDate date;
}
