package com.archive.data.dto;

import lombok.Data;

@Data
public class KeywordResponseDto {
    private String word;

}
