package com.archive.data.dto;

import com.archive.data.entity.ArticleEntity;
import lombok.Data;

import javax.persistence.Column;
import java.util.Set;

@Data
public class AuthorResponseDto {
    private String firstName;
    private String lastName;
    private String country;
    private int rate;
    private String description;
}
