package com.archive.data.entity;

import com.archive.data.dto.ArticleResponseDto;
import lombok.Data;

import javax.persistence.*;
import java.util.List;


@Data
@Entity
@Table(name = "authors")
public class AuthorEntity {
    @Id
    @Column(name = "author_id")
    @SequenceGenerator(
            name = "author_id_seq_Gen",
            sequenceName = "author_id_seq",
            allocationSize = 50,
            initialValue = 100
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "author_id_seq_Gen")
    private Long id;

    @Column(name = "fname")
    private String firstName;

    @Column(name = "lname")
    private String lastName;

    @Column(name = "country")
    private String country;

    @Column(name = "rate")
    private int rate;

    @Column(name = "description")
    private String description;

    @ManyToMany
    @JoinTable(name="authors_articles",
            joinColumns = @JoinColumn(name="author_id", referencedColumnName="author_id"),
            inverseJoinColumns = @JoinColumn(name="article_id", referencedColumnName="article_id")
    )
    private List<ArticleEntity> articles;

}
