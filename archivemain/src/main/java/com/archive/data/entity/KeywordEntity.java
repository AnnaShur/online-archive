package com.archive.data.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "keywords")
public class KeywordEntity {
    @Id
    @Column(name = "keyword_id")
    @SequenceGenerator(
            name = "keyword_id_seq_Gen",
            sequenceName = "keyword_id_seq",
            allocationSize = 50,
            initialValue = 10000
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "keyword_id_seq_Gen")
    private Long id;

    @Column(name = "word")
    private String word;

    @ManyToMany
    @JoinTable(name="articles_keywords",
            joinColumns = @JoinColumn(name="keyword_id", referencedColumnName="keyword_id"),
            inverseJoinColumns = @JoinColumn(name="article_id", referencedColumnName="article_id")
    )
    private List<ArticleEntity> articles;
}

