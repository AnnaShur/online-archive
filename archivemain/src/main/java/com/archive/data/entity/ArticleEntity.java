package com.archive.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;


@Data
@NoArgsConstructor
@Entity
@Table(name = "articles")
public class ArticleEntity {
    @Id
    @Column(name = "article_id")
    @SequenceGenerator(
            name = "article_id_seq_Gen",
            sequenceName = "article_id_seq",
            allocationSize = 50,
            initialValue = 1150
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "article_id_seq_Gen")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "link")
    private String link;

    @Column(name="date")
    private LocalDate date;

    @Column(name ="words_quant")
    private Long wordsQuantity;

    @NotNull
    @Column(name = "author_id")
    private Long authorId;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "articles")
    private List<AuthorEntity> authors;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "articles")
    private List<KeywordEntity> keywords;


}
    
    
