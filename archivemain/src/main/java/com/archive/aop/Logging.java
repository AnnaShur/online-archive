package com.archive.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class Logging {
    Logger log = LoggerFactory.getLogger(this.getClass());

    @Pointcut("execution(* com.archive.controllers.rest.ArticleController.getAllArticles())")
    public void getArticles() {
    }
    @Pointcut("execution(* com.archive.controllers.rest.ArticleController.*(..)))")
    public void articleController() {
    }
    @Pointcut("within(@org.springframework.stereotype.Controller *)")
    public void controller() {
    }
    @Pointcut("execution(* *.*(..))")
    protected void allMethod() {
    }
    @Before("getArticles()")
    public void beforeAdvice() {
        log.info("Article list was requested");
    }
    @After("execution(* com.archive.controllers.rest.ArticleController.restUploadFileAndJson())")
    public void afterUpload(){
        log.info("New article was uploaded");
    }
    @Around("articleController()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {

        long start = System.currentTimeMillis();
        try {
            String className = joinPoint.getSignature().getDeclaringTypeName();
            String methodName = joinPoint.getSignature().getName();
            Object result = joinPoint.proceed();
            long elapsedTime = System.currentTimeMillis() - start;
            log.info("Method " + className + "." + methodName + " ()" + " execution time : "
                    + elapsedTime + " ms");

            return result;
        } catch (IllegalArgumentException e) {
            log.error("Illegal argument " + Arrays.toString(joinPoint.getArgs()) + " in "
                    + joinPoint.getSignature().getName() + "()");
            throw e;
        }
    }






}