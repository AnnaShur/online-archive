package com.archive.controllers.rest;

import com.archive.data.dto.ArticleResponseDto;
import com.archive.data.dto.AuthorResponseDto;
import com.archive.data.entity.ArticleEntity;
import com.archive.data.entity.AuthorEntity;
import com.archive.service.api.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class AuthorController {

    private  final AuthorService authorService;

    @GetMapping("/getAllAuthorArticles")
    public List<ArticleResponseDto> getAllAuthorArticles(@RequestParam Long id) {
        return authorService.getAllAuthorArticles(id);
    }

    @GetMapping("/getAuthorById")
    public AuthorResponseDto getAuthorById(@RequestParam Long id) {
        return authorService.getAuthorById(id);
    }

    @GetMapping("/getByFirstOrLastName")
    public AuthorResponseDto getAuthorByFirstOrLastName(@RequestParam String name){
        return authorService.getByFirstNameOrLastName(name,name);
    }

    @GetMapping("/getByLastNameLike")
    public AuthorResponseDto getAuthorByLastName(@RequestParam String name){
        return authorService.findByLastNameLike(name);
    }
}
