package com.archive.controllers.rest;

import com.archive.data.dto.ArticleResponseDto;
import com.archive.data.entity.ArticleEntity;
import com.archive.data.mappers.ArticleMapper;
import com.archive.service.api.ArticleService;
import com.archive.service.api.KeywordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@RestController("/articles")
@Api("Article controller")
@RequiredArgsConstructor
public class ArticleController {

    private final KeywordService keywordService;
    private ArticleMapper mapper = Mappers.getMapper(ArticleMapper.class);

    @Qualifier("actualEntityService")
    private final ArticleService artService;

    @Autowired
    private RestTemplate restTemplate;

    @Operation(summary = "Say hello")
    @GetMapping("/hello")
    public String sayHello() {
        return "Hello everyone";
    }

    @Operation(summary = "Add article to DB")
    @ApiParam(
            name = "Article data",
            type = "body",
            value = "Article data that should be stored",
            example = "title: Programming," +
                    "text: https://javarush.ru/" +
                    "author_id: 1111",
            required = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "Wrong input format", content = @Content),
            @ApiResponse(responseCode = "500", description = "Wrong date or some required filds do not exists", content = @Content)})
    @PostMapping(value = "/add", consumes = "application/json")
    public ResponseEntity<String> add(@RequestBody ArticleResponseDto article) {
        ArticleEntity obj = artService.save(article);
        return ResponseEntity.ok("Data was saved successfully");
    }

    @ApiOperation(value = "Delete article by id",
            response = ArticleResponseDto.class,
            responseContainer = "List")
    @ApiParam(value = "Article id", required = true)

    @PostMapping("/deleteArticle")
    public void deleteArticle(@RequestParam Long id) {
        artService.delete(id);
    }

    @ApiOperation(value = "Get all articles from DB",
            response = ArticleResponseDto.class,
            responseContainer = "List")
    @GetMapping("/getAllArticles")
    public List<ArticleResponseDto> getAllArticles() {
        return artService.getAllArticles();
    }

    @GetMapping("/getByTimeInterval")
    public List<ArticleResponseDto> getArticlesByTime(@RequestParam String fromDate, String toDate) {
        return artService.getByTimeInterval(fromDate, toDate);
    }

    //TODO:example value
/*    @ApiParam(
            name =  "Date for article search",
            type = "string",
            value = "Date between which will be searched",
            example = "2020-01-01",
            required = true)*/
    @GetMapping(value = "/getByDateBetween")
    public List<ArticleResponseDto> getByDate(@RequestParam String fromDate, String toDate) {
        return artService.findAllByDateBetween(LocalDate.parse(fromDate), LocalDate.parse(toDate));
    }

    @GetMapping(value = "/getAllArticlesByWord", consumes = "text/plane")
    public List<ArticleResponseDto> getAllArticlesByWord(@RequestParam String word) {
        return keywordService.getAllArticlesByKeyword(word);
    }

    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<String> upload(@RequestPart(required = true, value = "file")
                                                 MultipartFile file) throws IOException {
        return artService.upload(file);
    }
    @PostMapping(value = "/uploadFileAndData", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<String> restUploadFileAndJson( @RequestPart(required = true, value = "file")
            MultipartFile file,@RequestPart String title,@RequestPart String authorId,@RequestPart String date) throws IOException {

        String link = restTemplate
                .postForEntity("http://localhost:8081/upload",
                        artService.createRequestEntity(file), String.class)
                .getBody();
        ArticleEntity resultEntity=new ArticleEntity();
        resultEntity.setAuthorId(Long.parseLong(authorId));
        resultEntity.setTitle(title);
        resultEntity.setLink(link);
        resultEntity.setDate(LocalDate.parse(date));
        artService.save(resultEntity);
        return ResponseEntity.ok("Data was saved");
    }

    @PostMapping(value = "/download")
    public  ResponseEntity < Resource >  getAllArticlesByWord(@RequestParam Long id) {
        ArticleEntity tmpEntity = artService.findArticleById(id);
        return restTemplate
                .postForEntity("http://localhost:8081/download",
                        tmpEntity.getLink(), Resource.class);

    }

    @GetMapping(value = "/resttest")
    public String restUpload() {
        return restTemplate.getForObject("http://localhost:8081/resttest",
                String.class);
    }

}


