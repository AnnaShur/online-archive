package com.archive.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("msg")
public class KafkaController {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;


    @PostMapping
    public void sendOrder(@RequestParam String msgId,@RequestParam String msg){
        kafkaTemplate.send("articles", msgId, msg);
    }
}
