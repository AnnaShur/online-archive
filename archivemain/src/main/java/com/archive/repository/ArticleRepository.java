package com.archive.repository;

import com.archive.data.dto.ArticleResponseDto;
import com.archive.data.entity.ArticleEntity;
import com.archive.service.api.ArticleService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

@Qualifier("ArticleRepository")
public interface ArticleRepository extends JpaRepository<ArticleEntity, Long> {

    List<ArticleEntity> findAllByDateBetween(LocalDate fromDate, LocalDate toDate);

}
