package com.archive.repository;

import com.archive.data.dto.AuthorResponseDto;
import com.archive.data.entity.AuthorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface AuthorRepository extends JpaRepository<AuthorEntity, Long> {

    @Query(value = "SELECT * FROM authors WHERE lname LIKE :#{#lastName}",nativeQuery = true)
    AuthorEntity findByLastNameLike(
            @Param("lastName") String lastName);

   AuthorEntity findByLastNameIgnoreCaseOrFirstNameIgnoreCase(String firstName, String lastName);
}
