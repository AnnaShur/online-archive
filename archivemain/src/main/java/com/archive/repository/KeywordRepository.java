package com.archive.repository;

import com.archive.data.entity.KeywordEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KeywordRepository extends JpaRepository<KeywordEntity, Long> {
    KeywordEntity findByWordIgnoreCase(String word);
}
