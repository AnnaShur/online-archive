package com.archive.uploader.data.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UploadFileResponseDto {
    private String filePath;
    private String fileType;
    private long size;
}
