package com.archive.uploader.services;

import com.archive.uploader.configuration.FileStorageProperties;
import io.swagger.v3.core.util.Json;
import lombok.AllArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.*;
import java.util.UUID;

@Service
@AllArgsConstructor
public class FileService {

    private final Path fileStorageLocation;
    private final Path downloadLocation;

    @Autowired
    public FileService(FileStorageProperties fileStorageProperties) throws IOException {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        Files.createDirectories(this.fileStorageLocation);
        this.downloadLocation = Paths.get(fileStorageProperties.getDownloadDir())
                .toAbsolutePath().normalize();

    }

    public String generateSubdirectory() throws IOException{
        UUID dirName=UUID.randomUUID();
        String realPathtoUploads = fileStorageLocation.toString()+"\\"+dirName+"\\";
        if(! new File(realPathtoUploads).exists()) {
            new File(realPathtoUploads).mkdir();
        }
        return dirName.toString();

    }
    public String storeFile(MultipartFile file) throws IOException {
        String newDirectoryName =generateSubdirectory();
        String filePath = fileStorageLocation.toString()+"\\"+newDirectoryName+"\\"+ file.getOriginalFilename();
        File dest = new File(filePath);
        file.transferTo(dest);
        return "/"+newDirectoryName+"/"+file.getOriginalFilename();
    }


    public ResponseEntity<Resource> downloadFile(String link,HttpServletRequest request) throws IOException {

        Path filePath = Paths.get(fileStorageLocation+link).normalize();
        Resource resource = new UrlResource(filePath.toUri());
        String  contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        if (contentType == null) {
            contentType = "application/octet-stream";
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);

        }

    }

