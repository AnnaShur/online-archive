package com.archive.uploader.controllers.rest;

import com.archive.uploader.services.FileService;
import io.swagger.v3.core.util.Json;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

@RestController

public class FileUploadController {

    @Autowired
     FileService fileService;


    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<String> uploadFile(@RequestPart(required = true, value = "file")
                                                     MultipartFile file) throws IOException {
        return  ResponseEntity.ok(fileService.storeFile(file));
    }

    @GetMapping(value = "/niceday")
    public String sayHello() {
        return "Have a nice day!";
    }

    @GetMapping(value = "/resttest")
    public String restTest() {
        return "Ho-ho!";
    }

   /* @PostMapping(value = "/download")
    public ResponseEntity<String> download (@RequestBody String link) throws IOException,MalformedURLException {
        return  ResponseEntity.ok(fileService.downloadFile(link));
    }*/
   @PostMapping(value = "/download")
   public ResponseEntity < Resource > download( @RequestBody String link, HttpServletRequest request) throws IOException {
       return  fileService.downloadFile(link,request);
   }

   String location="C:/Users/annas/OneDrive/Рабочий стол/Intellij Projects/me.jpg";

    @GetMapping("/downloadFile")
    public ResponseEntity < Resource > downloadFile( @RequestParam String link, HttpServletRequest request) throws MalformedURLException {
        Path filePath = Paths.get(location).normalize();
        Resource resource = new UrlResource(filePath.toUri());

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {

        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }





}
