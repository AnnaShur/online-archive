# Online-archive


Online-archive of scientific publications based on microservices. This project provides users with such features as searching, uploading, editing, deletting and downloading files in different formats (text, img and etc).They are stored and are avaliable any time. You can find articles of another authors which you are interested in by topic, author name, date or key words. 


### Brief functional description  


**Archivemain** project provides basic configuration, main operations with data, Docker and Kafka configuration,responses to user requests.
**uploader** projects performs service functions. It is a highly specialized microservice which is in duty of downloading files from server. It communicaates with **archivemain** by Spring Cloud Netflix Feign and is unreachable for users. 

